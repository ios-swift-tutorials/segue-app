//
//  SecondViewController.swift
//  SegueApp
//
//  Created by Rumeysa Bulut on 10.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var name = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = name
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
