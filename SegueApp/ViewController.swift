//
//  ViewController.swift
//  SegueApp
//
//  Created by Rumeysa Bulut on 10.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var firstViewController: UILabel!
    @IBOutlet weak var nameText: UITextField!
    
    var username  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        nameText.text = ""
    }

    @IBAction func nextClicked(_ sender: Any) {
        username = nameText.text!
        if username != "" {
            performSegue(withIdentifier: "toSecondVC", sender: nil)
        }
        else {
            let alert = UIAlertController(title: "Empty Name Text", message: "You should enter a name to go to the second page via Next button.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSecondVC" {
            let destinationVC = segue.destination as! SecondViewController
            destinationVC.name = username
        }
    }
}

